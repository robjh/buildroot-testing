################################################################################
#
# googletest
#
################################################################################

GOOGLETEST_VERSION = release-1.10.0
GOOGLETEST_SITE = https://github.com/google/googletest
GOOGLETEST_SITE_METHOD=git
GOOGLETEST_LICENSE = BSD-3-Clause
GOOGLETEST_LICENSE_FILES = LICENSE

HOST_GOOGLETEST_DEPENDENCIES = host-openssl host-util-linux

$(eval $(cmake-package))
$(eval $(host-cmake-package))
