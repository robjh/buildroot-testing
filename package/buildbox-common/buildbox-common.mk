################################################################################
#
# buildbox-common
#
################################################################################

BUILDBOX_COMMON_VERSION = 0.0.37
BUILDBOX_COMMON_SITE = https://gitlab.com/BuildGrid/buildbox/buildbox-common.git
BUILDBOX_COMMON_SITE_METHOD=git
BUILDBOX_COMMON_LICENSE = Apache-2.0
BUILDBOX_COMMON_LICENSE_FILES = LICENSE

HOST_BUILDBOX_COMMON_CFLAGS = -Wno-shadow -Wno-error=suggest-override
HOST_BUILDBOX_COMMON_CXXFLAGS = -Wno-shadow -Wno-error=suggest-override

BUILDBOX_COMMON_CFLAGS = $(TARGET_CFLAGS)
BUILDBOX_COMMON_CXXFLAGS = $(TARGET_CXXFLAGS)

HOST_BUILDBOX_COMMON_DEPENDENCIES = host-grpc host-util-linux host-protobuf

BUILDBOX__CONF_OPTS += \
	-DCMAKE_C_FLAGS="$(BUILDBOX_COMMON_CFLAGS)" \
	-DCMAKE_CXX_FLAGS="$(BUILDBOX_COMMON_CXXFLAGS)"

$(eval $(cmake-package))
$(eval $(host-cmake-package))
